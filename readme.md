# README #

This README documents the steps necessary to get Git up and running.

### What is this repository for? ###

* To get familiar with Git

### How do I get set up? ###

* Add this repository as a remote server (and fetch immediately)
* Checkout develop branch
* Create a feature branch, e.g. chuan-feature
* Add a HTML file containing an introduction of yourself in intro folder, e.g. intro/chuan.html
* Commit and push
* Create a pull request.

### Contribution guidelines ###

* Do not push to master branch directly.
* Write good commit messages.

### Who do I talk to? ###

* Repo owner or admin